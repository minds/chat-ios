// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
public extension VectorL10n {
  /// Choose from files
  static var imagePickerActionFiles: String { 
    return VectorL10n.tr("Untranslated", "image_picker_action_files") 
  }
  /// Profile picture
  static var onboardingAvatarAccessibilityLabel: String { 
    return VectorL10n.tr("Untranslated", "onboarding_avatar_accessibility_label") 
  }
  /// You can change this anytime.
  static var onboardingAvatarMessage: String { 
    return VectorL10n.tr("Untranslated", "onboarding_avatar_message") 
  }
  /// Add a profile picture
  static var onboardingAvatarTitle: String { 
    return VectorL10n.tr("Untranslated", "onboarding_avatar_title") 
  }
  /// Let's go
  static var onboardingCelebrationButton: String { 
    return VectorL10n.tr("Untranslated", "onboarding_celebration_button") 
  }
  /// Your preferences have been saved.
  static var onboardingCelebrationMessage: String { 
    return VectorL10n.tr("Untranslated", "onboarding_celebration_message") 
  }
  /// You’re all set!
  static var onboardingCelebrationTitle: String { 
    return VectorL10n.tr("Untranslated", "onboarding_celebration_title") 
  }
  /// Take me home
  static var onboardingCongratulationsHomeButton: String { 
    return VectorL10n.tr("Untranslated", "onboarding_congratulations_home_button") 
  }
  /// Your account %@ has been created.
  static func onboardingCongratulationsMessage(_ p1: String) -> String {
    return VectorL10n.tr("Untranslated", "onboarding_congratulations_message", p1)
  }
  /// Personalise profile
  static var onboardingCongratulationsPersonalizeButton: String { 
    return VectorL10n.tr("Untranslated", "onboarding_congratulations_personalize_button") 
  }
  /// Congratulations!
  static var onboardingCongratulationsTitle: String { 
    return VectorL10n.tr("Untranslated", "onboarding_congratulations_title") 
  }
  /// You can change this later
  static var onboardingDisplayNameHint: String { 
    return VectorL10n.tr("Untranslated", "onboarding_display_name_hint") 
  }
  /// Your display name must be less than 256 characters
  static var onboardingDisplayNameMaxLength: String { 
    return VectorL10n.tr("Untranslated", "onboarding_display_name_max_length") 
  }
  /// This will be shown when you send messages.
  static var onboardingDisplayNameMessage: String { 
    return VectorL10n.tr("Untranslated", "onboarding_display_name_message") 
  }
  /// Display Name
  static var onboardingDisplayNamePlaceholder: String { 
    return VectorL10n.tr("Untranslated", "onboarding_display_name_placeholder") 
  }
  /// Choose a display name
  static var onboardingDisplayNameTitle: String { 
    return VectorL10n.tr("Untranslated", "onboarding_display_name_title") 
  }
  /// Save and continue
  static var onboardingPersonalizationSave: String { 
    return VectorL10n.tr("Untranslated", "onboarding_personalization_save") 
  }
  /// Skip this step
  static var onboardingPersonalizationSkip: String { 
    return VectorL10n.tr("Untranslated", "onboarding_personalization_skip") 
  }
  /// This feature isn't available here. For now, you can do this with %@ on your computer.
  static func spacesFeatureNotAvailable(_ p1: String) -> String {
    return VectorL10n.tr("Untranslated", "spaces_feature_not_available", p1)
  }
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

